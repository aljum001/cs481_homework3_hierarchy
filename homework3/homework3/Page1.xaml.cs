﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

namespace homework3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
        }

        void BGSlider(object sender, ValueChangedEventArgs args)
        {
            L1.TextColor= Color.Black;
            double v = args.NewValue;
            int value = Convert.ToInt32(v);
            if(value==0)
            {
                colorSlider.BackgroundColor = Color.Blue;
            }
            else if (value == 1)
            {
                colorSlider.BackgroundColor = Color.Pink;
            }
            else if (value == 2)
            {
                colorSlider.BackgroundColor = Color.Red;
            }
            else if (value == 3)
            {
                colorSlider.BackgroundColor = Color.Green;
            }
            else if (value == 4)
            {
                colorSlider.BackgroundColor = Color.Violet;
            }
            else if (value == 5)
            {
                colorSlider.BackgroundColor = Color.DarkKhaki;
            }
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            await DisplayAlert("Nice", "You picked the right option", "OK");
            L1.Text = "Use the slider below to change the background color";
        }

        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            L1.Text = "Bye Bye";
            L1.FontSize = 20;
        }
    }
}