﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

namespace homework3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
            InitializeComponent ();
        }
        async void NewPage(Button Sender, EventArgs e)
        {
            //await DisplayAlert("See", "This is what you get for picking this.", "OK");
            MainPage.current = 4;
            await Navigation.PushAsync(new Page4());
        }

        void ChangeImg1(Button Sender, EventArgs e)
        {
            Img.Source = "https://s.pacn.ws/1500/s6/nintendo-switch-neon-redblue-507153.5.jpg?ojur03";
            B1.BackgroundColor = Color.Blue;
        }

        void ChangeImg2(Button Sender, EventArgs e)
        {
            Img.Source = "https://media.gamestop.com/i/gamestop/10141820_10141887_10167643_10167644_10167645_10167646_10167651_SCR23/Nintendo-Switch-with-Neon-Blue-and-Neon-Red-Joy-Con?$screen$";
            B2.BackgroundColor = Color.Blue;
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            await DisplayAlert("Really?", "How is that your favorite?", "OK");
        }

        private async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            B3.Text = "HAHA";
            await Task.Delay(500);
            await B3.RelRotateTo(360);
            await Task.Delay(500);
        }
    }

}