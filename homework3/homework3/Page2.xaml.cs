﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace homework3
{
    public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}

        void RotatePic(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            rotateImage.Rotation = value;
            int c = Convert.ToInt32(value);
            if ((c >= 0) && (c<120))
            {
                L1.Text = "White";
                L1.TextColor = Color.White;
            }
            else if((c >= 120) && (c<200))
            {
                L1.Text = "Red";
                L1.TextColor = Color.Red;
            }
            else if((c >=200) && (c<300))
            {
                L1.Text = "Green";
                L1.TextColor = Color.Green;
            }
            else if((c>=300) && (c<=360))
            {
                L1.Text = "Violet";
                L1.TextColor = Color.Violet;
            }
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            L1.Text = "White";
            await Task.Delay(500);
            await DisplayAlert("Hmmm", "Not the best but okay.", "OK");
        }

        private async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            L1.Text = "GoodBye";
            await Task.Delay(500);
            await L1.RelRotateTo(360); 
            await Task.Delay(1000);
        }
    }
}