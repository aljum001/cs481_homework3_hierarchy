﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

namespace homework3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page4 : ContentPage
	{
		public Page4()
		{
			InitializeComponent ();
        }

        async void Home(Button Sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            await DisplayAlert("See", "This is what you get for picking this.", "OK");
        }
    }
}