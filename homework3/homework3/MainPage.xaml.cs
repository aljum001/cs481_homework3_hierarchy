﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using System.Threading;
namespace homework3
{
    public partial class MainPage : ContentPage
    {
        bool LFlag;
        public static int current = 0;
        public MainPage()
        {
            InitializeComponent();
        }

        async void Page1(Button Sender, EventArgs e)
        {
            current = 1;
            await Navigation.PushAsync(new Page1());
        }

        async void Page2(Button Sender, EventArgs e)
        {
            
            current = 2;
            await Navigation.PushAsync(new Page2());
        }

        async void Page3(Button Sender, EventArgs e)
        {
            current = 3;
            await Navigation.PushAsync(new Page3());
        }

        void Last(Button Sender, EventArgs e)
        {
            LFlag = true;
            if (current == 1)
            {
                Navigation.PushAsync(new Page1());
            }
            else if (current == 2)
            {
                Navigation.PushAsync(new Page2());
            }
            else if (current == 3)
            {
                Navigation.PushAsync(new Page3());
            }
            else if (current == 4)
            {
                Navigation.PushAsync(new Page4());
            }
        }
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
                await Task.Delay(500);
                if (current == 1)
                {
                    B1.BackgroundColor = Color.Blue;
                }
                else if (current == 2)
                {
                    B2.BackgroundColor = Color.Green;
                }
                else if ((current == 3) || (current == 4))
                {
                    B3.BackgroundColor = Color.Gray;
                }
        }
        private async void ContentPage_Disappear(object sender, EventArgs e)
        {
            if (LFlag == true)
            {
                await B4.RelRotateTo(360);
                LFlag = false;
            }
            else
            {
                if (current == 1)
                {
                    await B1.RelRotateTo(360);
                    await Task.Delay(500);
                }
                else if (current == 2)
                {
                    await B2.RelRotateTo(360);
                    await Task.Delay(500);

                }
                else if (current == 3)
                {
                    await B3.RelRotateTo(360);
                    await Task.Delay(500);
                }
            }
            
        }
    }
}
